CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements / Discussion
 * Maintainers


INTRODUCTION
------------
This module creates a last login message to logged in users.

REQUIREMENTS / DISCUSSION
------------
This module functions as following:
 1) Display a block that shows the time the user last logged in.
 "You last logged in at"
 
 Discussion:
 -A block is created in last_login.module using hook_block_info().
 -The user object was used to store the last login time per user using
 hook_user_login() to record the login date / time.
 -The block is displayed with hook_block_view().
 
 2) In the user's current time-zone.
 
 Discussion:
 -The user's timezone was read and PHP functions used to display the last
 login.
 
 3) The block is automatically placed in the left side-rail region.
 
 Discussion:
 -The sidebar_first region was used. This is set in hook_block_info(). This
 can be changed for different themes.
 
 4) The block is not visible for non-logged in users.
 
 Discussion:
 -The drupal function user_is_logged_in() was used to determine if the the user
 was logged in our not preventing the display to anonymous users.
 -This was configured in hook_block_info().
 
 5) Allow the message to be configured per user.
 
 Discussion:
 -The user profile form was modifed using hook_form_alter(). The new login
 message was saved into the user object.
 
 6) Frontend Javascript collapses (animated) the block after 10
 seconds.
 
 Discussion:
 -A javascript file was included with the module and defined with
 hook_block_view(). The js/last_login.js file contains jQuery to collapse the
 block in an animated fashion after 10 seconds. CSS and more jQuery/javascript
 could be worked on here to improve UX.
 
 7) Creates a panes ctools content type (pane) in addition to a standard
 block.
 
 Discussion:
 -The ctools content type (pane) was defined starting with
 hook_ctools_plugin_directory() hook.
 -Standard boiler plate hooks were used to define this pane.
 -The user object is accessed with the render callback
 last_login_content_type_render function.
 
MAINTAINERS
-----------
Current maintainers:
 * Ben LeDuc (bpleduc) - https://drupal.org/u/bpleduc
