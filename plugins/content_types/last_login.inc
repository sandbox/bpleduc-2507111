<?php
/**
 * @file
 * Include file for last_login module.
 */

/*
 * Define our pane for Last Login.
 */
$plugin = array(
  'title' => t('Last Login'),
  'description' => t('Displays a last login time.'),
  'single' => TRUE,
  'content_types' => array('last_login'),
  'render callback' => 'last_login_content_type_render',
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'edit form' => 'last_login_content_type_edit_form',
  'category' => array(t('Panel Examples'), -9),
);

/**
 * Ctools edit form.
 */
function last_login_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['last_login_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('The last login label.'),
    '#default_value' => !empty($conf['last_login_label']) ? $conf['last_login_label'] : 'Last Login',
  );
  return $form;
}

/**
 * Ctools edit form submit handler.
 */
function last_login_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('last_login_label') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Render callback function.
 */
function last_login_content_type_render($subtype, $conf, $args, $context) {

  $block = new stdClass();
  global $user;

  $login = (!empty($user->data['login']) ? $user->data['login'] : 'You last logged in at');
  $last = (!empty($user->data['lastlogin']) ? $user->data['lastlogin'] : 'Today');

  $block->content = 'Our content here';
  $block->content = array(
    '#markup' => t("@login: @date", array('@login' => $login, '@date' => $last)),
  );

  return $block;
}
