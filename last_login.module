<?php
/**
 * @file
 * Module file for last_login module.
 */

/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function last_login_block_info() {

  $blocks['last_login'] = array(
    // info: The name of the block.
    'info' => t('Last Login'),
    'status' => TRUE,
    'region' => 'sidebar_first',
    'visibility' => BLOCK_VISIBILITY_PHP,
    'pages' => '<?php return user_is_logged_in(); ?>',
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function last_login_block_view($delta = '') {
  // The $delta parameter tells us which block is being requested.
  global $user;

  switch ($delta) {
    case 'last_login':
      $login = (!empty($user->data['login']) ? $user->data['login'] : 'You last logged in at');
      $last = (!empty($user->data['lastlogin']) ? $user->data['lastlogin'] : 'Today');
      $block['subject'] = t("@login: @date", array('@login' => $login, '@date' => $last));
      $block['content'] = array(
        '#attached' => array(
          'js' => array(
            drupal_get_path('module', 'last_login') . '/js/last_login.js',
          ),
        ),
      );
      break;
  }
  return $block;
}

/**
 * Implements hook_user_login().
 */
function last_login_user_login(&$edit, $account) {
  date_default_timezone_set($account->timezone);
  $date = date('m-d-Y H:i:s');

  $edit = array(
    'data' => array(
      'currlogin' => $date,
      'lastlogin' => (!empty($account->data['currlogin']) ? $account->data['currlogin'] : $date),
    ),
  );

  user_save($account, $edit);
}

/**
 * Implements hook_form_alter().
 */
function last_login_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'user_profile_form':

      $form['account']['login'] = array(
        '#type' => 'textfield',
        '#title' => t('Your Login Prompt'),
        '#default_value' => (!empty($form['#user']->data['login']) ? $form['#user']->data['login'] : 'You last logged in at'),
      );
      $form['#submit'][] = 'last_login_custom_submission';

      break;
  }

}

/**
 * Callback for user_profile_form submit.
 */
function last_login_custom_submission($form, &$form_state) {

  $user = user_load($form['#user']->uid);

  $edit = array(
    'data' => array(
      'login' => $form_state['values']['login'],
    ),
  );

  user_save($user, $edit);
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function last_login_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && !empty($plugin)) {
    return "plugins/$plugin";
  }
}
