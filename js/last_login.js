/**
 * @file
 * Javscript file for last_login module.
 */

(function ($) {

  $(document).ready(function(){
    $("#block-last-login-last-login").css("overflow", "hidden");
    setTimeout(function() {
        $("#block-last-login-last-login").animate({
          height: "0px"
        }, 1000, function() {
          // Animation complete.
          $("#block-last-login-last-login").css("border", "0");
          $("#block-last-login-last-login").css("padding", "0");
        });
    }, 10000);
  });

}(jQuery));
